import os, sys, datetime, time
import logging, logging.config
from urllib import urlencode
from urllib2 import urlopen, URLError

# Setup environment to pull in django models
sys.path.append(os.path.join(os.path.abspath('..')))
sys.path.append(os.path.join(os.path.abspath('..'), 'webPearl'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'webPearl.settings'
from webPearl.pearl.models import Event

class IndividualEvent(object):
  def __init__(self, start, end):
    self.start = start or None
    self.end = end or None

  def __str__(self):
    try:
      startDescription = self.start.description
    except AttributeError:
      startDescription = 'None'
      
    try:
      endDescription = self.end.description
    except AttributeError:
      endDescription = 'None'

    return "Start: %s\nEnd: %s" % (startDescription, endDescription)


def dispatchEvent(event):
  ''' Sends out an event '''
  log = logging.getLogger('dispatcher')
  
  from settings import *
  #api_url = 'http://api.tropo.com/1.0/sessions' #settings.api_url #'http://api.tropo.com/1.0/sessions'
  #token = '41da7856b830ee41a42a47b24db3e462a64f359699c43187b177967bc179a0610a4a2709ac27ac645600693e' #settings.token #'41da7856b830ee41a42a47b24db3e462a64f359699c43187b177967bc179a0610a4a2709ac27ac645600693e'
  number = '+17174138746'
  
  #print api_url
  #return

  try:
    startMsg = event.start.description
  except AttributeError:
    startMsg = 'None'
  try:
    endMsg = event.end.description
  except AttributeError:
    endMsg = 'None'
  
  log.info('Start: %s, End: %s' % (startMsg, endMsg))
  
  params = urlencode([
    ('token', token),
    ('action', 'create'),
    ('numberToDial', number),
    ('startMsg', startMsg),
    ('endMsg', endMsg),
    ('test', 'this is a test')])

  # Send information to the webservice
  success = False
  attempts = 0
  
  while attempts < 3 and not success:
    try:
      data = urlopen('%s?%s' % (api_url, params)).read()
      success = True
    except URLError(e):
      if hasattr(e, 'reason'):
        log.error('Dispatcher failed to reach a server')
        log.error('Reason: %s' % e.reason)
      elif hasattr(e, 'code'):
        log.error("The server couldn't fulfill the request")
        log.error('Error code: %s' % e.code)
        
      attempts += 1


def findEvents(dateTime):
  ''' 
  Finds events that are starting and stopping at dateTime 
  
  Returns: List of IndividualEvent objects for that specific time
  '''
  log = logging.getLogger('dispatcher')
  events = {}
  
  # Find ending events
  try:
    endEvents = Event.objects.filter(end=dateTime)
    for endEvent in endEvents:
      log.debug('Found: %s' % endEvent.description)
      try:
        startEvent = Event.objects.get(start=dateTime, user=endEvent.user)
        log.debug('Found: %s' % startEvent.description)
      except Event.DoesNotExist:
        startEvent = None
      
      events[endEvent.user] = IndividualEvent(startEvent, endEvent)
      
  except Event.DoesNotExist:
    pass
  
  # Find starting events that were missed
  try:
    startEvents = Event.objects.filter(start=dateTime)
    for startEvent in startEvents:
      if startEvent.user not in events:
        events[startEvent.user] = IndividualEvent(startEvent, None)
      else:
        continue
    
  except Event.DoesNotExist:
    pass
    
  return events
    
    


def main():
  logging.config.fileConfig('logging.conf')
  log = logging.getLogger('dispatcher')

  while (1):
    now = datetime.datetime.now()
    thisMinute = datetime.datetime(now.year, now.month, now.day, now.hour, now.minute)
    #exampleDate = datetime.datetime(2010, 12, 16, 11, 59)
    events = findEvents(thisMinute)

    if events:
      log.info('Found %s events' % (len(events)))
      for key, val in events.items():
        log.info('Dispatching: %s' % val)
        dispatchEvent(val)
    else:
       log.info('No Events')

    time.sleep(60)
  
if __name__ == "__main__":
  main()
