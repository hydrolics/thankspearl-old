#import unittest

import sys, os, datetime

# Setup environment to pull in django models
sys.path.append(os.path.join(os.path.abspath('..')))
sys.path.append(os.path.join(os.path.abspath('..'), 'webPearl'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'webPearl.settings'
from webPearl.pearl.models import Event

from django.test.utils import setup_test_environment
setup_test_environment()

from django.utils import unittest
import dispatcher


class findEvent(unittest.TestCase):
  
  def testStartEvent(self):
    ''' Tests that it finds an example start '''
    exampleDate = datetime.datetime(2010, 12, 4, 21, 00)
    result = findEvents(exampleDate)
    self.assertEqual(result, True)
    
  def testNotFound(self):
    ''' Tests that it returns None if the date is not found '''
    exampleDate = datetime.datetime(1982,12,27,9,28)  # I couldn't have an event when I was born!
    result = dispatcher.findEvents(exampleDate)
    self.assertEqual(result, None)
  
if __name__ == "__main__":
  unittest.main()
