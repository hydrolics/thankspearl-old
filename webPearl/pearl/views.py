from django.http import HttpResponse

from tropo import Tropo, Session


from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

import grammar
import simplejson 

@csrf_exempt
def tropo(request):
  '''
      The tropo webservice calls this after dispatcher.py initiates a call
  '''
  
  if request.method == 'POST':
    json_data = simplejson.loads(request.raw_post_data)  # Tropo embeds JSON in the POST parameters
    try:
      t = Tropo() 
      t.call(to=json_data['session']['parameters']['numberToDial']) # Call the user

      # Greet them and tell them what they need to do
      t.say(grammar.greeting('Brad'))
      t.say("<break time='0.75s' />")
      t.say("Hopefully you finished %s" % json_data['session']['parameters']['endMsg'])
      t.say("<break time='1s' />")
      t.say("Time to start %s" % json_data['session']['parameters']['startMsg'])

    except KeyError:
      print 'Error: Key not present in JSON'

  print "Sending: %s" % t.RenderJson()
  return HttpResponse(t.RenderJson(), mimetype='application/json') 

