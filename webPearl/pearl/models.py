from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Event(models.Model):
  user = models.ForeignKey(User)
  start = models.DateTimeField()
  end = models.DateTimeField()
  description = models.CharField(max_length=255)
  
  created = models.DateTimeField(auto_now=True)
  updated = models.DateTimeField(auto_now_add=True)
  
  def __unicode__(self):
    return "%s" % (self.description)
  
  
