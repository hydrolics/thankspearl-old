import random

def greeting(name=None):
  ''' 
      Generates a string with a random textual greeting
      Returns: string containing a textual greeting
  '''
  
  salutations = [
    "Hello",
    "Hi",
    "Hi there",
    "How are you?",
    "How are you doing?",
    "How's it going?",
    "Howdy",
    "Good day"]
  
  named_greetings = ["Hello %s"]
  
  noname_greetings = [
    "Hi, looks like we have some work to do.",
    "Hello"]

  if name is not None:
    return "%s, %s." % (salutations[random.randint(0,len(salutations)-1)], name)
  else:
    return salutations[random.randint(0,len(salutations)-1)]
    
    
if __name__ == "__main__":
  greeting('Brad')
