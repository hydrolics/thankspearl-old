from pearl.models import Event
from django.contrib import admin



class EventAdmin(admin.ModelAdmin):
  list_display = ('start', 'end', 'description')
  list_filter = ('start', 'end')
  
  
admin.site.register(Event, EventAdmin)
